#include <stdio.h>
#include <string.h>
/* lower: convert c to lower case; ASCII only */

int lower(int c)
{
    if (c >= 'A' && c <= 'Z')
        return c + 'a' - 'A';
    else
        return c;
}

int main()
{
    char a;
    printf("Input a character: ");
    scanf("%s", &a);
    printf("This is your current input: %c\n", a);
    char character = lower(a);
    printf("The value of your character, when convertd to lowercase, is: %d\n", character);
}

/* Sample Output
Input a character: i
This is your current input: i
The value of your character, when convertd to lowercase, is: 105
*/
