#include <stdio.h>

int main()
{
    int i = 5, j;
    j = i++;
    printf("after postfix increment i=%d j=%d", i, j);

    i = 5;
    j = ++i;
    printf("\nafter prefix increment i=%d j=%d", i, j);
    return 0;
}

/* Output:
after postfix increment i=6 j=5
after prefix increment i=6 j=6
*/
