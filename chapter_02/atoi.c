#include <stdio.h>

/* atoi: convert s to integer */

int atoi(char s[])
{
    int i, n;

    n = 0;
    for (i = 0; s[i] >= '0' && s[i] <= '9'; ++i)
        n = 10 * n + (s[i] - '0');                      /* s[i] - '0' gives the numeric value of the character stored in s[i] */
    return n;
}

int main()
{
    int num = atoi("characters");
    printf("The value of n is %d\n", num);

}
