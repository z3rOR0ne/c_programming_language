#include <stdio.h>
#include <string.h>

const char msg[] = "message.";

int main()
{
    printf("warning: %s\n", msg);
    printf("The length of the warning message: %d\n", strlen(msg));
    return 0;
}
