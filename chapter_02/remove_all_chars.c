#include <stdio.h>
#include <string.h>

/* this is a more eloquent solution to the C Programming Language Book's squeeze() funciton found on page 47 of the book */
/* it demonstrates the use of pointers in the book, but does not use pointers to hold the value of the string, and thusly fails */

/* this solution was found on Stack Overflow, and actually does what the book's squeeze() function is supposed to do */
/* https://stackoverflow.com/questions/9895216/how-to-remove-all-occurrences-of-a-given-character-from-string-in-c */


/* create a function called remove_all_chars that expects a pointer called str, and a character variable called c */
void remove_all_chars(char* str, char c)
{
    /* create a pointer called pr, that expects a char type, that is equal to the value of str, */
    /* and also create a pointer called pw, that expects a char type, that is equal to the value of str */
    char *pr = str, *pw = str;
    /* while the pointer pr exists... */
    while(*pr) {
        /* the pointer pw becomes equal to the pointer of the next value of the string located at the pointer of pr */
        *pw = *pr++;
        /* the value of pw then becomes equal to the pointer address of any character that does not equal to the value of the character c */
        pw += (*pw != c);
    }
    /* if the pointer to the pr stops existing (we've reached the end of the str) */
    /* the last character is given a null terminator character */
    *pw = '\0';
}

int main()
{
    /* create a string with an excessive amount of the character 'l'*/
    char str[] = "llHello, world!ll";
    /* call remove_all_chars() function and pass it the string array as well as the character we wish to remove*/
    remove_all_chars(str, 'l');
    /* and finally print the edited string */
    printf("'%s'\n", str);
    return 0;
}

/* Outputs:
'Heo, word!'
*/
