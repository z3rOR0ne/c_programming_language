#include <stdio.h>

int main()
{
    int sum = 17, count= 5;
    double mean;

    /* here we "coerce" the int variable sum by "casting" its type as a double */
    /* although type coersion can be done implicitly by the C compiler, */
     /* it can also be expliclty done in this manner */

    mean = (double) sum / count;
    printf("Value of mean: %f\n", mean);
}

/*Output:
Value of mean: 3.400000
*/
