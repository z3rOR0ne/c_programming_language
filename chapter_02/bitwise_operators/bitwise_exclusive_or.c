/* 12 = 00001100 (In Binary) */
/* 25 = 00011001 (In Binary) */

/* Bitwise XOR (Exclusive OR) Operation of 12 and 25 */
  /* 00001100 */
/* ^ 00011001 */
  /* -------- */
  /* 00010101 = 21 (In decimal) */

/* The Bitwise Exclusive OR (otherwise known as XOR) converts the bit to a 1 if the corresponding compared bits are opposites of each other (0 if two 0s or two 1s are compared, 1 if they are opposite). */

#include <stdio.h>
int main()
{
    int a = 12, b = 25;
    printf("Output = %d\n", a^b);
    return 0;
}

/* Output: */
/* Output = 21 */
