
/* Two examples here, first is a demonstration of what happens with the unary (~) operator, which simply converts the bits of a single operand to its opposite bits.*/

/* 35 = 00100011 (In Binary) */

/* Bitwise complement Operation of 35 */
/* ~ 00100011 */
  /* -------- */
  /* 11011100 = 220 (In decimal) */

/* Twist in bitwise complement operator in C Programming */

/* The bitwise complement of 35 (~35) is -36 instead of 220, but why? */

/* For any integer n, bitwise complement of n will be -(n+1). To understand this, you should have the knowledge of 2s complement. */

/* 2's complement */

/* Two's complement is an operation on binary numbers. The 2's complement of a number is equal to the complement of that number plus 1. For example: */

/* ---------------------------------------------------------------------------------- */
    /* Decimal             Binary          2's complement */
        /* 0                 00000000          -(11111111+1) = -00000000 = -0(decimal) */
        /* 1                 00000001          -(11111110+1) = -11111111 = -256(decimal) */
        /* 12                00001100          -(11110011+1) = -11110100 = -244(decimal) */
        /* 220               11011100          -(00100011+1) = -00100100 = -36(decimal) */

/* Note: Overflow is ignored while computing 2's complement. */
/* ---------------------------------------------------------------------------------- */

/* Bitwise complement of any number N is -(N+1). Here's how: */

/* bitwise complement of N = ~N (represented in 2's complement form) */
/* 2's complement of ~N= -(~(~N)+1) = -(N+1) */

#include <stdio.h>
int main()
{
    printf("Output = %d\n", ~35);
    printf("Output = %d\n", ~-12);
    return 0;
}

/* Output: */
/* Output = -36 */
/* Output = 11 */

