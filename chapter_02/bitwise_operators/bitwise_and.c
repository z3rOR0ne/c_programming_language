/* 12 = 00001100 (In Binary) */
/* 25 = 00011001 (In Binary) */

/* Bit Operation of 12 and 25 */
  /* 00001100 */
/* & 00001101 */
  /* -------- */
  /* 00001000 = 8 (In decimal) */

/* note how above whenever there is a 0 compared to the 1, it is converted to a 0 */
/* but if there is two 1s, then it is converted to a 1, this is essentially how the bitwise AND operator worsk, it compares the two bits, compares them, and converts it to a 1 ONLY if there is NO 0 */

#include <stdio.h>

int main()
{
    int a = 12, b = 25;
    printf("Output = %d\n", a&b); //a&b is the actual bitwise AND operation
    return 0;
}

/* Output: */
/* Output = 8 */

