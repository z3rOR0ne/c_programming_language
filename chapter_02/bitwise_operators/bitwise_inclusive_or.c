/* 12 = 00001100 (In Binary) */
/* 25 = 00011001 (In Binary) */

/* Bitwise OR Operation of 12 and 25 */
  /* 00001100 */
/* | 00011001 */
  /* -------- */
  /* 00011101 = 29 (In decimal) */

/* Similar to the AND operator, only reversed, if a 1 is present, it defaults to a 1 when compared using the OR operator */

#include <stdio.h>
int main()
{
    int a = 12, b = 25;
    printf("Output = %d\n", a|b); // the actual OR operation
    return 0;
}

/* Output:
Output = 29
*/
