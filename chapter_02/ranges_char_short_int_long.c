/**
 * C program to find range of data type
 */

#include <stdio.h>

void printUnsignedRange(int bytes)
{
    int bits = 8 * bytes;

    unsigned long long to = (1LLU << (bits - 1)) + ((1LL << (bits - 1)) - 1);;

    printf(" 0 to %llu\n\n", to);
}

void printSignedRange(int bytes)
{
    int bits = 8 * bytes;

    long long from  = -(1LL << (bits - 1));
    long long to    =  (1LL << (bits - 1)) - 1;

    printf(" %lld to %lld\n\n", from, to);
}

int main()
{
    printf("Range of char =");
    printSignedRange(sizeof(char));

    printf("Range of unsigned char =");
    printUnsignedRange(sizeof(unsigned char));

    printf("Range of short =");
    printSignedRange(sizeof(short));

    printf("Range of unsigned short =");
    printUnsignedRange(sizeof(unsigned short));

    printf("Range of int =");
    printSignedRange(sizeof(int));

    printf("Range of unsigned int =");
    printUnsignedRange(sizeof(unsigned int));

    printf("Range of long =");
    printSignedRange(sizeof(long));

    printf("Range of unsigned long =");
    printUnsignedRange(sizeof(unsigned long));

    printf("Range of long long =");
    printSignedRange(sizeof(long long));

    printf("Range of unsigned long long =");
    printUnsignedRange(sizeof(unsigned long long));

    return 0;
}
/* Output:
Range of char = -128 to 127

Range of unsigned char = 0 to 255

Range of short = -32768 to 32767

Range of unsigned short = 0 to 65535

Range of int = -2147483648 to 2147483647

Range of unsigned int = 0 to 4294967295

Range of long = -9223372036854775808 to 9223372036854775807

Range of unsigned long = 0 to 18446744073709551615

Range of long long = -9223372036854775808 to 9223372036854775807

Range of unsigned long long = 0 to 18446744073709551615
*/
