/* file copying */

/* read a character */
/* while (character is not end-of-file indicator) */
    /* output the character just read */
    /* read a character */

#include <stdio.h>

/* copy input to output; 1st version */
int main()
{
    int c;

    c = getchar();
    while (c != EOF) {
        putchar(c);
        c = getchar();
    }
}

/* Whatever input is put into the keyboard is echoed back, */
/* returning you to a prompt to further type some more and repeat */
/* until you end the program by hitting CTRL + C. */
