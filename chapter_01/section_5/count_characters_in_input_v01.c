#include <stdio.h>

/* count characters in input; 1st version */

int main()
{
    long nc;

    nc = 0;
    while (getchar() != EOF)
        ++nc; // increment by 1
    printf("%ld\n", nc);
}

/* NOTE: in order to exit this gracefully, you must use CTRL+D, not CTRL+C */
