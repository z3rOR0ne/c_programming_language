#include <stdio.h>

/* count lines in input */

int main()
{
    int c, n1, b1, t1;

    n1 = 0;
    while ((c = getchar()) != EOF)
        if (c == '\n')
            ++n1;
        else if (c == ' ')
            ++b1;
        else if (c == '\t')
            ++t1;
    printf("\nThe number of lines inputted: %d\n", n1);
    printf("The number of blank spaces inputted: %d\n", b1);
    /* tab spaces count doesn't work currently... */
    printf("The number of tab spaces inputted: %d\n", t1);
}
