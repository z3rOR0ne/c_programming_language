#include <stdio.h>

/* print Celsius-Fahrenheight table */

int main()
{
    int fahr, celsius;
    int lower, upper, step;

    lower = 0;          /* lower limit of temperature table */
    upper = 300;        /* upper limit */
    step = 20;          /* step size */

    celsius = lower;
    printf("Celsius\t\tFahrenheight\n");
    while (celsius <= upper) {
        fahr = (celsius * 1.8) + 32;
        printf("%d\t\t%d\n", celsius, fahr);
        celsius = celsius + step;
    }
}

/* Outputs:
Celsius		Fahrenheight
0		32
20		68
40		104
60		140
80		176
100		212
120		248
140		284
160		320
180		356
200		392
220		428
240		464
260		500
280		536
300		572
*/
